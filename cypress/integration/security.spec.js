context('Actions', () => {

    const ADMIN_EMAIL = "admin@api-sensor.com";
    const ADMIN_PASSWORD = "azerty";

    const USER_EMAIL = "sample@api-sensor.com";
    const USER_PASSWORD = "azerty";

    beforeEach(() => {
        cy.visit('/login');

        cy.intercept('GET', '/admin/sensor').as('sensor_index');
    })

    it('allows logging in with an admin user', () => {
        cy.get('input[name=email]').type(ADMIN_EMAIL);
        cy.get('input[name=password]').type(ADMIN_PASSWORD);
        cy.get('form').submit();

        cy.location('pathname').should('have.string', '/admin/sensor');
    });

    it('allows logging in with a sample user', () => {
        cy.get('input[name=email]').type(USER_EMAIL);
        cy.get('input[name=password]').type(USER_PASSWORD);
        cy.get('form').submit();

        cy.location('pathname').should('have.string', '/admin/sensor');
        cy.wait('@sensor_index').its('response.statusCode', {timeout: 0}).should('eq', 403);
    });
});