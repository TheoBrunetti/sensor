<?php

namespace App\Repository;

use App\Entity\Data;
use App\Entity\Sensor;
use App\Object\RequestParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @method Data|null find($id, $lockMode = null, $lockVersion = null)
 * @method Data|null findOneBy(array $criteria, array $orderBy = null)
 * @method Data[]    findAll()
 * @method Data[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Data::class);
    }

    public function retrieveLastDate(Sensor $sensor)
    {
        $result = $this->createQueryBuilder('d')
            ->leftJoin("d.sensor", "s")
            ->select('d.timestamp')
            ->andWhere('s.devEUI = :devEUI')
            ->setParameter('devEUI', $sensor->getDevEUI())
            ->orderBy('d.timestamp', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
        if(count($result) > 0) {
            return $result[0]['timestamp'];
        }
        return new \DateTime('2021-06-30');
    }

    protected function addValue(QueryBuilder $qb, $valueName)
    {
        $qb->addSelect('d.'.$valueName);
    }

    protected function addAgregateFunction(QueryBuilder $qb, string $valueName, string $function, string $name)
    {
        $qb->addSelect(sprintf('%s(d.%s) as %s', $function, $valueName, $name));
    }


    protected function addAgregate(QueryBuilder $qb, string $format, string $valueName)
    {
        switch($format) {
            case RequestParameter::FORMAT_AVERAGE:
                $this->addAgregateFunction($qb, $valueName, 'avg', 'average');
                break;
            case RequestParameter::FORMAT_MAXIMUM:
                $this->addAgregateFunction($qb, $valueName, 'max', 'maximum');
                break;
            case RequestParameter::FORMAT_MINIMUM:
                $this->addAgregateFunction($qb, $valueName, 'min', 'minimum');
                break;
        }
    }

    protected function addAgregateDatetime(QueryBuilder $qb)
    {
        $qb->addSelect('DATE_FORMAT(min(d.timestamp), \'%Y-%m-%dT%H:%i:%s\') as timestamp');
    }

    protected function selectDatas(RequestParameter $parameter, QueryBuilder $qb)
    {
        $valueName = $parameter->getValue();
        $qb->select('\'device\' as object, s.devEUI as object_id, s.type as object_type');
        switch ($parameter->getFormat())
        {
            default;
            case RequestParameter::FORMAT_VALUE:
                $this->addValue($qb, $valueName);
                $qb->addSelect('DATE_FORMAT(d.timestamp, \'%Y-%m-%dT%H:%i:%s\') as timestamp');
                break;
            case RequestParameter::FORMAT_ALL:
                $this->addValue($qb, $valueName);
                $this->addAgregate($qb, RequestParameter::FORMAT_AVERAGE, $valueName);
                $this->addAgregate($qb, RequestParameter::FORMAT_MINIMUM, $valueName);
                $this->addAgregate($qb, RequestParameter::FORMAT_MAXIMUM, $valueName);
                $this->addAgregateDatetime($qb);
                break;
            case RequestParameter::FORMAT_AVERAGE:
                $this->addAgregate($qb, RequestParameter::FORMAT_AVERAGE, $valueName);
                $this->addAgregateDatetime($qb);
                break;
            case RequestParameter::FORMAT_MINIMUM:
                $this->addAgregate($qb, RequestParameter::FORMAT_MINIMUM, $valueName);
                $this->addAgregateDatetime($qb);
                break;
            case RequestParameter::FORMAT_MAXIMUM:
                $this->addAgregate($qb, RequestParameter::FORMAT_MAXIMUM, $valueName);
                $this->addAgregateDatetime($qb);
                break;
        }
    }

    protected function whereDatas(RequestParameter $parameter, QueryBuilder $qb)
    {
        $qb->andWhere(sprintf("d.%s IS NOT NULL", $parameter->getValue()));
        if($parameter->getBegin()) {
            $qb->andWhere("d.timestamp >= :begin")
            ->setParameter("begin", $parameter->getBegin()->format('Y-m-d\TH:i:s.u'));
        }
        if($parameter->getEnd()) {
            $qb->andWhere("d.timestamp < :end")
                ->setParameter("end", $parameter->getEnd()->format('Y-m-d\TH:i:s.u'));
        }
        if(count($parameter->getSensors())) {
            $orX = $qb->expr()->orX();

            foreach ($parameter->getSensors() as $sensor) {
                $orX->add($qb->expr()->eq('s.id', $sensor->getId()));
            }
            $qb->andWhere($orX);
        }
    }

    protected function isInterval(RequestParameter $parameter)
    {
        return $parameter->getFormat() != RequestParameter::FORMAT_VALUE;
    }

    protected function groupbyDatas(RequestParameter $parameter, QueryBuilder $qb)
    {
        if(!$this->isInterval($parameter)) {
            return;
        }
        $interval = sprintf("%d", $parameter->getInterval());
        $qb->groupBy("d.sensor");
        $qb->addSelect("date(d.timestamp) as HIDDEN day");
        $qb->addGroupBy('day');
        $qb->addOrderBy('day');
        if($parameter->getInterval() > 0) {
            $qb->addSelect("floor(cast(hour(d.timestamp) as unsigned) / ".$interval.") as HIDDEN dinterval");
            $qb->addGroupBy('dinterval');
            $qb->addOrderBy('dinterval');
            $qb->addSelect("concat(DATE_FORMAT(min(d.timestamp), '%Y-%m-%dT'), case when (cast(hour(min(d.timestamp)) as unsigned) < 10) then '0' else '' end, cast((floor(cast(hour(d.timestamp) as unsigned) / ".$interval.")) * ".$interval." as char), ':00:00') as begin");
            $qb->addSelect("concat(DATE_FORMAT(max(d.timestamp), '%Y-%m-%dT'), case when (cast(hour(max(d.timestamp)) as unsigned) < 10) then '0' else '' end, cast((floor(cast(hour(d.timestamp) as unsigned) / ".$interval.") + 1) * ".$interval." - 1  as char), ':59:59') as end");
        } else {
            $qb->addSelect("concat(DATE_FORMAT(min(d.timestamp), '%Y-%m-%dT'), '00:00:00') as begin");
            $qb->addSelect("concat(DATE_FORMAT(max(d.timestamp), '%Y-%m-%dT'), '23:59:59') as end");
        }
    }

    public function retrieveDatas(RequestParameter $parameter):?array
    {
        $qb = $this->createQueryBuilder('d');
        $qb->leftJoin("d.sensor", "s");
        $this->selectDatas($parameter, $qb);
        $this->whereDatas($parameter, $qb);
        $this->groupByDatas($parameter, $qb);
        $qb->addOrderBy('object_id');
        $qb->addOrderBy('d.timestamp');

        return $qb->getQuery()->getResult();
    }
}
