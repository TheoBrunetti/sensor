<?php

namespace App\Entity;

use App\Repository\SensorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SensorRepository::class)
 */
class Sensor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $devEUI;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity=Data::class, mappedBy="sensor")
     */
    private $datas;

    public function __construct()
    {
        $this->datas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDevEUI(): ?string
    {
        return $this->devEUI;
    }

    public function setDevEUI(string $devEUI): self
    {
        $this->devEUI = $devEUI;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|Data[]
     */
    public function getDatas(): Collection
    {
        return $this->datas;
    }

    public function addDatas(Data $datas): self
    {
        if (!$this->datas->contains($datas)) {
            $this->datas[] = $datas;
            $datas->setSensor($this);
        }

        return $this;
    }

    public function removeDatas(Data $datas): self
    {
        if ($this->datas->removeElement($datas)) {
            // set the owning side to null (unless already changed)
            if ($datas->getSensor() === $this) {
                $datas->setSensor(null);
            }
        }

        return $this;
    }
}
