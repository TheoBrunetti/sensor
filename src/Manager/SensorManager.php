<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 13/08/21
 * Time: 11:39
 */

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Sensor;
use App\Entity\Data;
use App\Repository\DataRepository;

class SensorManager
{
    private $em;
    private $cookie;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->cookie = 'G5JuWvdJFls0jvQwwpgYFhrogGoPB4il9NDBkDkhqnptceiZGMj9N9nPg6j3cWhJ';
    }

    // generic function curl
    private function CallAPI($method, $url, $header = null, $data = false, $login = null, $password = null)
    {
        $curl = curl_init();

        if($header) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        if($login and $password) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $login . ":" . $password);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    protected function updateSensor($sensor, $sensorJson)
    {
        $sensor->setType($sensorJson->deviceType->code);
        $sensor->setLatitude($sensorJson->servicePoint->location->lat);
        $sensor->setLongitude($sensorJson->servicePoint->location->lon);
    }

    protected function createSensor($sensorJson)
    {
        $sensor = new Sensor();
        $sensor->setDevEUI($sensorJson->devEUI);
        $this->em->persist($sensor);
        $this->updateSensor($sensor, $sensorJson);
        return $sensor;
    }

    protected function makeRequest($uri)
    {
        $url = "https://chambery.requea.com/rest/device/v1/".$uri;
        dump($url);
        $headers = [];
        $headers[] = "X-API-Key: ".$this->cookie;
        return $this->CallAPI("GET", $url, $headers);
    }

    protected function setData(Data $data, $dataJson, $property, $setter)
    {
        if(property_exists($dataJson->data, $property)) {
            call_user_func_array(array($data, $setter), array($dataJson->data->$property));
        }
    }

    public function updateDatas(Sensor $sensor)
    {
        $now = new \DateTime('now');

        /** @var  $repository DataRepository */
        $repository = $this->em->getRepository('App:Data');
        $startDate = $repository->retrieveLastDate($sensor);

        $response = $this->makeRequest($sensor->getDevEUI().'/data?startDate='.$startDate->format('Y-m-d\TH:i:s.u'));

        $datas = json_decode($response);
        foreach($datas as $dataJson) {
            $data = new Data();
            $data->setSensor($sensor);
            $data->setTimestamp(new \DateTime($dataJson->dateTime));
            $data->setCreatedAt($now);
            $data->setLatitude($sensor->getLatitude());
            $data->setLongitude($sensor->getLongitude());

            $this->setData($data, $dataJson, "temperature", 'setTemperature');
            $this->setData($data, $dataJson, "head_temperature", 'setTemperature');
            $this->setData($data, $dataJson, "pressure", 'setPressure');
            $this->setData($data, $dataJson, "humidity", 'setHumidity');

            $this->em->persist($data);
        }
    }

    public function updateSensors()
    {

        $response = $this->makeRequest('search');
        $sensors = json_decode($response);

        foreach ($sensors as $sensorJson) {
            $devEUI = $sensorJson->devEUI;

            $sensor = $this->em->getRepository('App:Sensor')
                ->findOneBydevEUI($devEUI);
            if(!$sensor) {
                $sensor = $this->createSensor($sensorJson);
            } else {
                $this->updateSensor($sensor, $sensorJson);
            }
            $this->updateDatas($sensor);
        }
        $this->em->flush();

        return $response;
    }
}