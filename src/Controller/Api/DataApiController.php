<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 16/08/21
 * Time: 14:18
 */

namespace App\Controller\Api;

use App\Object\RequestParameter;
use App\Repository\DataRepository;
use App\Repository\SensorRepository;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/api", name="api_")
 */
class DataApiController extends AbstractController
{
    protected function apiResponse(EntityManagerInterface $em, $parameters)
    {
        /** @var  $repository DataRepository */
        $repository = $em->getRepository('App:Data');

        $datas = $repository->retrieveDatas($parameters);

        /*
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $response = new JsonResponse();
        $response->setContent($serializer->serialize($datas, 'json'));

        return $response;
        */

        return new JsonResponse($datas);
    }
    /**
     * @Route("/v1/datas", name="sensor_datas")
     */
    public function retrieveDatas(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $parameters = new RequestParameter($em, $request);

        return $this->apiResponse($em, $parameters);
    }

    /**
     * @Route("/v1/datas/devices/{devEUI}", name="deveui_sensor_datas")
     */
    public function retrieveDevice(Request $request, EntityManagerInterface $em, $devEUI, SensorRepository $sensorRepository): JsonResponse
    {
        $parameters = new RequestParameter($em, $request);
        $sensor = $sensorRepository->findOneBydevEUI($devEUI);
        $parameters->addSensor($sensor);

        return $this->apiResponse($em, $parameters);
    }
}
