<?php

namespace App\Tests\Object;

use App\Entity\Sensor;
use App\Object\RequestParameter;
use App\Repository\SensorRepository;
use Doctrine\ORM\EntityManager;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

class RequestParameterTest extends KernelTestCase
{
    public function setUp(): void
    {
        self::bootKernel();
    }

    /**
     * Mocks & stubs example
     */
    public function testAddDevice(): void
    {
        $request = Request::createFromGlobals();

        $sensor = new Sensor();
        $sensor->setDevEUI('XB67');

        $sensorRepository = $this->createMock(SensorRepository::class);
        $sensorRepository->expects($this->any())
            ->method('findOneBydevEUI')
            ->willReturn($sensor)
        ;

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($sensorRepository)
        ;

        $requestParameter = new RequestParameter($objectManager, $request);
        $requestParameter->addDevice('XB67');

        $this->assertEquals($requestParameter->getSensors()->first()->getDevEUI(), 'XB67');
    }

    public function testAddDevices(): void
    {
        $request = Request::createFromGlobals();
        $request->query->set('device', '70B3D57BA00025F5');

        $objectManager = static::getContainer()->get('doctrine.orm.entity_manager');

        $requestParameter = new RequestParameter($objectManager, $request);
        $requestParameterReflection = new ReflectionClass($requestParameter);

        $methodReflection = $requestParameterReflection->getMethod('addDevices');
        $methodReflection->setAccessible(true);
        $methodReflection->invoke($requestParameter);

        $this->assertEquals($requestParameter->getSensors()->first()->getDevEUI(), '70B3D57BA00025F5');
    }

    public function testAddSensor(): void
    {
        $request = Request::createFromGlobals();
        $objectManager = $this->createMock(EntityManager::class);

        $requestParameter = new RequestParameter($objectManager, $request);
        $requestParameter->addSensor(new Sensor());

        $this->assertEquals($requestParameter->getSensors()->count(), 1);
    }

    public function testDefaultFormat(): void
    {
        $request = Request::createFromGlobals();
        $objectManager = $this->createMock(EntityManager::class);

        $requestParameter = new RequestParameter($objectManager, $request);

        $this->assertEquals($requestParameter->getFormat(), RequestParameter::FORMAT_ALL);
    }
}
